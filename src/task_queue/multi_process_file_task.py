from threading import Thread

from common.const import CONST
from common.globals import Globals
from common.loggers.logger import log
from common.wrappers.redis_queue import RedisQueue
from settings.setting import SETTING


class MultiProcessHandle(object):
    def __init__(self):
        self.thread_list = []
        self.thread_num = int(SETTING.MAX_THREAD_NUM)

    def release(self):
        error_thread_list = []
        for process_thread in self.thread_list:
            try:
                log.info('try to release thread')
                process_thread.stop()
            except Exception as e:
                log.error('release error=[{}]'.format(str(e)))
                error_thread_list.append(process_thread)
                continue
        self.thread_list.clear()
        if error_thread_list:
            self.thread_list.extend(error_thread_list)

    def start(self):
        log.info('dispatch manager try to start all threads,thread_num:{}'.format(self.thread_num))
        try:
            for i in range(self.thread_num):
                log.info('try to create thread:{}'.format(i + 1))
                process_thread = SingleThread()
                self.thread_list.append(process_thread)
                process_thread.start()
        except Exception as e:
            log.error('start error=[{}]'.format(str(e)))
            self.release()
            pass

    def repair(self):
        log.info('dispatch manager try to repair')
        dead_thread_list = []
        try:
            for process_thread in self.thread_list:
                if not process_thread.is_alive():
                    dead_thread_list.append(process_thread)
            for c_thread in dead_thread_list:
                process_thread = SingleThread()
                self.thread_list.append(process_thread)
                process_thread.start()
                self.thread_list.remove(c_thread)
        except Exception as e:
            log.error('start error=[{}]'.format(str(e)))

    def get_status(self):
        log.info('dispatch manager try to get status')
        num_of_alive = 0
        num_of_dead = 0

        for process_thread in self.thread_list:
            if process_thread.is_alive():
                num_of_alive += 1
            else:
                num_of_dead += 1
        return num_of_alive, num_of_dead

    def stop(self):
        log.info('dispatch manager try to stop')
        self.release()


class SingleThread(object):
    def __init__(self):
        self.is_running = False
        self.file_task_queue = RedisQueue(CONST.FILE_PROCESS, Globals.get_redis_wrapper().get_redis_conn())

    def process_file(self):
        while self.is_running:
            try:
                pass
            except Exception as e:
                log.exception(str(e))

    def start(self):
        log.info('dispatch thread start')
        self.is_running = True
        Thread(target=self.process_file).start()
        return

    def stop(self):
        self.is_running = False
        log.info('dispatch thread is go to stop')
        return

    def is_alive(self):
        return self.is_running
