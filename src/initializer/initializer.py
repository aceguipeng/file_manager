# -*- coding:utf-8 -*-

from common.loggers.logger import log
from initializer.base import BS_INITIALIZER
from monitor.proc_monitor import ProcessMonitor


def init_executor():
    ProcessMonitor.start()


def de_init_executor():
    try:
        ProcessMonitor.stop()
    except Exception as error:
        log.info('stop process monitor error: {}'.format(error))


def init():
    try:
        BS_INITIALIZER.initialize()
        init_executor()
    except Exception as e:
        log.error('{}'.format(str(e)))
        raise e


def de_init():
    BS_INITIALIZER.de_initialize()
    de_init_executor()
