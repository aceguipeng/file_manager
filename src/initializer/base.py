# -*- coding:utf-8 -*-
import os

from common.const import CONST
from common.globals import Globals
from common.loggers.logger import log
from common.wrappers.etcd_wrapper import EtcdWrapper
from common.wrappers.hbase_wrapper import HbaseWrapper
from common.wrappers.mongo_wrapper import MongoWrapper
from common.wrappers.mysql_wrapper import MysqlWrapper
from common.wrappers.redis_wrapper import RedisWrapper
from settings.setting import SETTING


class _BaseInitializer:
    def __init__(self):
        self.has_been_initializer = False
        self.base_dir = '.'

    @classmethod
    def _init_redis(cls):
        redis_wrapper = RedisWrapper(host=SETTING.REDIS_HOST, port=SETTING.REDIS_PORT, password=SETTING.REDIS_PASSWORD)
        Globals.set_redis_wrapper(redis_wrapper)
        log.info('initialize redis successfully.')

    @classmethod
    def _init_etcd(cls):
        etcd_wrapper = EtcdWrapper(host=os.environ.get(CONST.ETCD_HOST, '192.168.0.244'),
                                   port=int(os.environ.get(CONST.ETCD_PORT, '2379')))
        etcd_wrapper.connect_etcd_cluster()
        Globals.set_etcd_wrapper(etcd_wrapper)

    def _init_setting(self):
        SETTING.set_base_dir(self.base_dir)
        SETTING.reload_from_local()
        SETTING.reload_from_remote(Globals.get_etcd_wrapper())

        log.info('initialize SETTING successfully.')

    @classmethod
    def _init_hbase(cls):
        hbase_wrapper = HbaseWrapper(SETTING.HBASE_HOST,
                                     SETTING.HBASE_TIMEOUT)
        hbase_wrapper.connect(table_prefix=CONST.SYSTEM_PROJECT_NAME)
        hbase_wrapper.create_all_tables()
        Globals.set_hbase_wrapper(hbase_wrapper)
        log.info('initialize hbase successfully.')

    @classmethod
    def _init_mysql(cls):
        mysql_wrapper = MysqlWrapper()
        mysql_wrapper.connect_mysql()
        Globals.set_mysql_wrapper(mysql_wrapper)
        log.info('initialize mysql successfully.')

    @classmethod
    def _init_mongo(cls):
        mongo_wrapper = MongoWrapper()
        mongo_wrapper.connect_mongo()
        Globals.set_mongo_wrapper(mongo_wrapper)
        log.info('initialize mongodb successfully')

    def initialize(self):
        log.info('try to initialize the base wrappers.')
        if self.has_been_initializer:
            log.info('system management has been initialized yet.')
            return

        # Attention: Don't change the order for initializing component.
        # env --> etcd --> setting --> redis --> stat
        self._init_etcd()
        self._init_setting()
        self._init_redis()
        self._init_mongo()
        # self._init_hbase()
        # self._init_mysql()

        log.info('base initialize successfully.')
        self.has_been_initializer = True

    def de_initialize(self):
        try:
            log.info('try to de-initialize the system management.')
            if not self.has_been_initializer:
                log.info('base has not been initialized.')
                return
            self.has_been_initializer = False
            log.info('base de initialize successfully.')
        except Exception as err:
            log.info('stop base initializer error {}'.format(err))


BS_INITIALIZER = _BaseInitializer()
