class PageInfo(object):
    def __init__(self, counters, page_size):
        self.page_size = page_size
        self.page_no = 0
        self.counters = counters
        self.pages = self.get_pages(counters, page_size)

    @staticmethod
    def get_pages(counters, page_size):
        if counters % page_size == 0:
            pages = counters // page_size
        else:
            pages = counters // page_size + 1

        return pages
