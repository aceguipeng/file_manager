# -*- coding: utf-8 -*-
import csv
import os
import shutil

from common.const import CONST
from common.globals import Globals


class _PathManager(object):
    def __init__(self):
        self.column_put_path = None
        self.short_put_path = None
        self.out_put_dir = None
        self.out_put_path = None
        self.wrong_put_path = None
        self.trash_put_path = None
        self.result_put_path = None
        self.condition = None
        self.country = None
        self.counter = {}
        self.black_list = []

    def init_path_manager(self, input_file_path: str, condition: dict):
        index = condition[CONST.INDEX]
        self.counter[index] = 0

        input_file_name = input_file_path.split('/')[-1]
        file_name = input_file_name.split('.')[0]
        file_path = '/'.join(input_file_path.split('/')[:-1])
        out_put_file_name = '{}_{}.txt'.format(file_name, index)
        wrong_file_name = '{}_wrong.txt'.format(file_name)
        short_file_name = '{}_short.txt'.format(file_name)
        column_file_name = '{}_column.txt'.format(file_name)
        self.out_put_dir = '{}/splits/{}'.format(file_path, file_name)

        if not os.path.exists(self.out_put_dir):
            os.makedirs(self.out_put_dir, exist_ok=True)
        else:
            shutil.rmtree(self.out_put_dir)
            os.makedirs(self.out_put_dir, exist_ok=True)

        self.out_put_path = '{}/{}'.format(self.out_put_dir, out_put_file_name)
        self.wrong_put_path = '{}/{}'.format(self.out_put_dir, wrong_file_name)
        self.column_put_path = '{}/{}'.format(self.out_put_dir, column_file_name)
        self.short_put_path = '{}/{}'.format(self.out_put_dir, short_file_name)
        self.condition = condition

    def change_output_path(self, input_file_path):
        index_value = self.condition[CONST.INDEX]
        input_file_name = input_file_path.split('/')[-1]
        file_path = '/'.join(input_file_path.split('/')[:-1])
        if self.counter.get(index_value) is None or self.counter[index_value] == 0:
            out_put_file_name = '{}_{}.txt'.format(input_file_name.split('.')[0], index_value)
        else:
            out_put_file_name = '{}_{}({}).txt'.format(input_file_name.split('.')[0], index_value,
                                                       self.counter[index_value])
        self.out_put_dir = '{}/splits/{}'.format(file_path, input_file_name.split('.')[0])
        if not os.path.exists(self.out_put_dir):
            os.makedirs(self.out_put_dir, exist_ok=True)
        else:
            pass
        self.out_put_path = '{}/{}'.format(self.out_put_dir, out_put_file_name)

    def change_output_path_by_operation(self, input_file_path, operation: str):
        input_file_name = input_file_path.split('/')[-1]
        file_path = '/'.join(input_file_path.split('/')[:-1])
        file_name = input_file_name.split('.')[0]
        out_put_file_name = '{}_{}.txt'.format(file_name, operation)
        result_put_file_name = '{}_result.txt'.format(file_name)
        wrong_file_name = '{}_wrong.txt'.format(file_name)
        self.out_put_dir = '{}/{}/{}'.format(file_path, operation, file_name)
        if not os.path.exists(self.out_put_dir):
            os.makedirs(self.out_put_dir, exist_ok=True)
        else:
            pass
        self.out_put_path = '{}/{}'.format(self.out_put_dir, out_put_file_name)
        self.result_put_path = '{}/{}'.format(self.out_put_dir, result_put_file_name)
        self.wrong_put_path = '{}/{}'.format(self.out_put_dir, wrong_file_name)

    def set_black_list(self):
        redis_conn = Globals.get_redis_wrapper().get_redis_conn()
        input_file = '../data/black_list/black_email_list_sorted.txt'
        with open(input_file, 'r', encoding=CONST.LATIN) as file:
            reader = csv.reader(file)
            for fields in reader:
                if fields != "":
                    email = fields[0]
                    self.black_list.append(email)
                else:
                    continue
        redis_conn.set(CONST.SUM_EMAILS, 0)
        redis_conn.set(CONST.ERROR_EMAILS, 0)
        return

    def write_result(self):
        redis_conn = Globals.get_redis_wrapper().get_redis_conn()
        sum_emails = int(redis_conn.get(CONST.SUM_EMAILS))
        error_emails = int(redis_conn.get(CONST.ERROR_EMAILS))
        error_percent = '{}%'.format(round(error_emails / sum_emails, 4) * 100)
        message = 'sum_emails: {}\nerror_emails: {}\nerror_percent: {}'.format(sum_emails, error_emails,
                                                                               error_percent)
        with open(self.result_put_path, 'w') as file:
            file.write(message)

        # clear result
        redis_conn.set(CONST.SUM_EMAILS, 0)
        redis_conn.set(CONST.ERROR_EMAILS, 0)
        return


PathManager = _PathManager()
