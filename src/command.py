# -*- coding: utf-8 -*-
import csv
import sys
import time

from common.loggers.logger import log
from initializer.initializer import init, de_init
from service.mongo_service import check_match_emails

csv.field_size_limit(sys.maxsize)


def start_all():
    init()


def stop_all():
    de_init()


if __name__ == '__main__':
    start_all()
    start_time = int(round(time.time()))
    # input_dir = "/home/data/su_data/us_voters/connecticut/format/check_ok/connecticutVoters_format/fix"
    # log.info('begin to start commanding,command file: {}'.format(input_dir.split('/')[-1]))
    # dispatch_files(input_dir)
    # remove_error_email(input_dir)
    # split_file_by_email_index(input_dir)
    # check_file_format_and_split(input_dir)
    # distinct(input_dir)
    # split_by_country(input_dir)
    check_match_emails()
    end_time = int(round(time.time()))
    log.info('DONE!!:use time :{} s'.format(end_time - start_time))
    stop_all()
