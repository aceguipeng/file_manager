# -*- coding: utf-8 -*-
import csv
import os
import re
import sys

from common.const import CONST
from common.loggers.logger import log
from bean.path_manager import PathManager
from service.dir_files import get_dir_files, clear_empty_file
from settings.setting import SETTING

TWO_AT_EMAIL_PATT = re.compile('^[^@]+@[^@]+@[^@]+$')
Z_PATT = re.compile('\x1a')
EMAIL_PATT = re.compile('email')
NOR_EMAIL_SUB = re.compile(r'[^a-zA-Z]')
N_PATT = re.compile(r'\n')

csv.field_size_limit(sys.maxsize)


def check_email(email_field):
    email_field = Z_PATT.sub("", email_field)
    flag = False
    if email_field.find("@") >= 0:
        flag = True
    # deal email has two
    if TWO_AT_EMAIL_PATT.match(email_field) or email_field == "EMail":
        flag = True

    return flag


def write_cache_fields(cache_lines, input_file, field_patt):
    output_file = open(PathManager.out_put_path, 'a')
    wrong_put_file = open(PathManager.wrong_put_path, 'a')
    short_put_file = open(PathManager.short_put_path, 'a')
    column_put_file = open(PathManager.column_put_path, 'a')

    output_writer = csv.writer(output_file)
    wrong_writer = csv.writer(wrong_put_file)
    short_writer = csv.writer(short_put_file)
    column_writer = csv.writer(column_put_file)

    for fields in cache_lines:
        if len(fields) <= 2:
            log.debug('fields length is less than 3,error_info: {}'.format(','.join(fields)))
            short_writer.writerow(fields)
            continue
        else:
            pass

        try:
            email_field = fields[PathManager.condition[CONST.INDEX]]
        except IndexError:
            if len(fields) <= 0:
                log.error('fields is empty...')
            else:
                PathManager.condition[CONST.INDEX] = len(fields) - 1
                while PathManager.condition[CONST.INDEX] >= 0:
                    email_field = fields[PathManager.condition[CONST.INDEX]]
                    result = field_patt.match(email_field)
                    if result:
                        PathManager.change_output_path(input_file)
                        # change new path
                        output_file.close()
                        output_file = open(PathManager.out_put_path, 'a')
                        output_writer = csv.writer(output_file)
                        break
                    else:
                        PathManager.condition[CONST.INDEX] -= 1
                        continue
                if PathManager.condition[CONST.INDEX] < 0:
                    if is_column(fields):
                        column_writer.writerow(fields)
                    else:
                        log.error('index error,error_info: {}'.format(','.join(fields)))
                        wrong_writer.writerow(fields)
                    continue
                else:
                    pass

        except Exception as e:
            log.exception('Error in fields: {}'.format(','.join(fields)), str(e))
            continue

        result = field_patt.match(email_field)
        if result:
            output_writer.writerow(fields)
        else:
            cache_index = {}
            for index, value in enumerate(fields):
                result = field_patt.match(value)
                if result:
                    cache_index[CONST.INDEX] = index

            dict_index = cache_index.get(CONST.INDEX)
            if dict_index:
                PathManager.condition[CONST.INDEX] = dict_index
                PathManager.change_output_path(input_file)

                # change new path
                output_file.close()
                output_file = open(PathManager.out_put_path, 'a')
                output_writer = csv.writer(output_file)
                output_writer.writerow(fields)
            else:
                email_field_parts = email_field.split(" ")
                if len(email_field_parts) > 1:
                    for i in email_field_parts:
                        result = field_patt.match(i)
                        if result:
                            new_fields = [i] + fields[1:]
                            output_writer.writerow(new_fields)
                        else:
                            continue
                else:
                    if is_column(fields):
                        column_writer.writerow(fields)
                    else:
                        wrong_writer.writerow(fields)
                    break

    output_file.close()
    wrong_put_file.close()
    short_put_file.close()
    column_put_file.close()


def split_file_by_email_index_file(input_file):
    condition = {
        CONST.INDEX: 0,
        CONST.PATTERN: CONST.EMAIL_PATTERN
    }
    PathManager.init_path_manager(input_file, condition)

    field_patt = re.compile(PathManager.condition[CONST.PATTERN])
    cache_lines = []

    with open(input_file, 'r', encoding=CONST.LATIN) as file:
        reader = csv.reader(file)

        for fields in reader:
            format_lines = format_fields(fields)
            if len(format_lines) <= 1:
                if len(cache_lines) <= SETTING.CACHE_LINE_SIZE:
                    cache_lines.append(fields)
                    continue
                else:
                    write_cache_fields(cache_lines, input_file, field_patt)
                    cache_lines.clear()
            else:
                for line in format_lines:
                    if line != "":
                        fields = ','.split(line)
                        cache_lines.append(fields)

        if len(cache_lines) > 0:
            write_cache_fields(cache_lines, input_file, field_patt)
            cache_lines.clear()


def format_fields(fields):
    line = ','.join(fields)
    line_parts = N_PATT.split(line)
    return line_parts


def is_column(fields: list):
    column_flag = False
    for field in fields:
        field = field.lower()
        field = NOR_EMAIL_SUB.sub("", field)
        if EMAIL_PATT.match(field):
            column_flag = True
            break

    return column_flag


def split_file_by_email_index(input_file):
    if os.path.isfile(input_file):
        split_file_by_email_index_file(input_file)
        clear_empty_file(PathManager.out_put_dir)
    else:
        file_list = get_dir_files(input_file)
        for input_file in file_list:
            split_file_by_email_index_file(input_file)
            clear_empty_file(PathManager.out_put_dir)
