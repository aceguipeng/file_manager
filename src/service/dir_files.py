# -*- coding: utf-8 -*-
import os
import shutil


def get_dir_files(input_dir):
    files = os.listdir(input_dir)
    file_paths = []
    for file in files:
        file_path = os.path.join(input_dir, file)
        if os.path.isfile(file_path):
            file_paths.append(file_path)
    return file_paths


def clear_dir_file(output_dir):
    if not os.path.exists(output_dir):
        os.makedirs(output_dir, exist_ok=True)
    else:
        shutil.rmtree(output_dir)
        os.makedirs(output_dir, exist_ok=True)


def clear_empty_file(output_dir):
    file_list = get_dir_files(output_dir)
    for file in file_list:
        if os.path.isfile(file):
            if os.path.getsize(file) <= 0:
                os.remove(file)
            else:
                pass
        else:
            pass
