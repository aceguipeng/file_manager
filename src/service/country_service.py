# -*- coding: utf-8 -*-
import csv
import os
import re

from common.const import CONST
from bean.path_manager import PathManager
from service.dir_files import get_dir_files, clear_dir_file, clear_empty_file
from settings.setting import SETTING

EMAIL_SUB_COMMA = re.compile(r',')
NO_ALPHA_COMMA = re.compile(r'[^0-9a-zA-Z\s]')
NUM_PATT = re.compile(r'^[0-9]*')
ALPHA_PATT = re.compile(r'[a-zA-Z]+')


def split_by_country(input_dir):
    if os.path.isfile(input_dir):
        input_file_name = input_dir.split('/')[-1]
        file_name = input_file_name.split('.')[0]
        file_path = '/'.join(input_dir.split('/')[:-1])
        country_dir = '{}/split_by_country/{}'.format(file_path, file_name)
        clear_dir_file(country_dir)
        dispatch_by_country(input_dir)
        clear_empty_file(country_dir)
    else:
        file_list = get_dir_files(input_dir)
        for file in file_list:
            input_file_name = file.split('/')[-1]
            file_name = input_file_name.split('.')[0]
            file_path = '/'.join(file.split('/')[:-1])
            country_dir = '{}/split_by_country/{}'.format(file_path, file_name)
            clear_dir_file(country_dir)
            dispatch_by_country(file)
            clear_empty_file(country_dir)


def dispatch_by_country(input_file):
    cache_lines = []
    with open(input_file, 'r', encoding=CONST.LATIN) as file:
        reader = csv.reader(file)

        for fields in reader:
            if len(cache_lines) <= SETTING.CACHE_LINE_SIZE:
                cache_lines.append(fields)
                continue
            else:
                write_cache_fields(cache_lines, input_file)
                cache_lines.clear()

        if len(cache_lines) > 0:
            write_cache_fields(cache_lines, input_file)
            cache_lines.clear()


def write_cache_fields(cache_lines, input_file):
    PathManager.country = None
    country_file = None
    for fields in cache_lines:
        fields = list(map(lambda x: x.strip(), fields))
        fields = change_country_file(input_file, fields)
        if country_file:
            country_file.close()
            country_file = open(PathManager.out_put_path, 'a')
            country_writer = csv.writer(country_file)
        else:
            country_file = open(PathManager.out_put_path, 'a')
            country_writer = csv.writer(country_file)

        country_writer.writerow(fields)


def change_country_file(input_file_path, fields: list):
    city = fields[CONST.CITY_INDEX]
    state_index = CONST.STATE_INDEX
    country_index = CONST.COUNTRY_INDEX
    zip_index = CONST.ZIP_INDEX
    country_id = None
    state_id = None
    if len(fields) == 10:
        pass
    elif len(fields) == 12:
        state_index = +1
        country_index += 2
        zip_index += 1

        country_id = fields[CONST.COUNTRY_ID_INDEX]
        state_id = fields[CONST.STATE_ID_INDEX]
    elif len(fields) == 11:
        country_index = 9
        zip_index = 8
        state_index = 7
    else:
        raise IndexError

    state = fields[state_index]
    country = fields[country_index]
    zip_code = fields[zip_index]

    input_file_name = input_file_path.split('/')[-1]
    file_name = input_file_name.split('.')[0]
    file_path = '/'.join(input_file_path.split('/')[:-1])

    if country != "":
        sub_country = NO_ALPHA_COMMA.sub("", country)
        if sub_country in CONST.COUNTRY_DICT:
            fields.remove(country)
            country = CONST.COUNTRY_DICT[sub_country]
            fields.insert(country_index, country)
        else:
            country = 'other_country'

        out_put_file_name = '{}_{}.txt'.format(file_name, country)
    elif zip_code != "":
        if ALPHA_PATT.match(zip_code):
            out_put_file_name = '{}_{}.txt'.format(file_name, 'has_zip_nor_num')
        else:
            out_put_file_name = '{}_{}.txt'.format(file_name, 'has_zip_num')
    elif state != "":
        out_put_file_name = '{}_{}.txt'.format(file_name, 'has_state')
    elif city != "":
        out_put_file_name = '{}_{}.txt'.format(file_name, 'has_city')
    elif country_id:
        out_put_file_name = '{}_{}.txt'.format(file_name, 'has_country_id')
    elif state_id:
        out_put_file_name = '{}_{}.txt'.format(file_name, 'has_state_id')
    else:
        out_put_file_name = '{}_{}.txt'.format(file_name, 'other')

    PathManager.out_put_dir = '{}/split_by_country/{}'.format(file_path, file_name)
    if not os.path.exists(PathManager.out_put_dir):
        os.makedirs(PathManager.out_put_dir, exist_ok=True)
    else:
        pass
    PathManager.out_put_path = '{}/{}'.format(PathManager.out_put_dir, out_put_file_name)
    PathManager.country = country
    return fields
