# -*- coding: utf-8 -*-
import csv
import os
import re

from common.const import CONST
from common.loggers.logger import log
from common.utils import trip_line_space
from bean.path_manager import PathManager
from service.dir_files import get_dir_files, clear_dir_file
from settings.setting import SETTING

EMAIL_SUB_COMMA = re.compile(r',')
CACHE_SUB_PATT = re.compile(r'[^0-9a-zA-Z\u00C0-\u00FF]')


def distinct(input_dir):
    if os.path.isfile(input_dir):
        distinct_file(input_dir)
    else:
        file_list = get_dir_files(input_dir)
        for file_path in file_list:
            try:
                if os.path.isfile(file_path):
                    distinct_file(file_path)
                else:
                    pass
            except Exception as e:
                log.exception(str(e))


def distinct_file(input_file):
    init_distinct_output_path(input_file)
    cache_lines = []

    with open(input_file, 'r', encoding=CONST.LATIN) as file:
        reader = csv.reader(file)
        for fields in reader:
            try:
                if len(cache_lines) <= SETTING.CACHE_LINE_SIZE:
                    cache_lines.append(fields)
                    continue
                else:
                    write_cache_fields(cache_lines)
                    cache_lines.clear()
            except Exception as e:
                log.exception('Error in handle: {},error_info: {}'.format(fields, str(e)))
                continue
        if len(cache_lines) > 0:
            write_cache_fields(cache_lines)
            cache_lines.clear()


def write_cache_fields(cache_lines):
    dis_cache = set()
    with open(PathManager.out_put_path, 'a') as file:
        file_writer = csv.writer(file)
        for fields in cache_lines:
            is_same_info = True
            fields = trip_line_space(fields, EMAIL_SUB_COMMA)
            if len(dis_cache) == 0:
                # no cache then write in cache,continue next
                line = ','.join(fields)
                dis_cache.add(line)
                continue
            else:
                # has cache then compare
                cache_line = dis_cache.pop()
                line = ','.join(fields)
                if is_same_line(line, cache_line):
                    cache_fields = EMAIL_SUB_COMMA.split(cache_line)
                    cache_fields_filter = list(filter(lambda x: x != "", cache_fields))
                    fields_filter = list(filter(lambda x: x != "", fields))
                    cache_fields_filter_size = len(cache_fields_filter)
                    fields_filter_size = len(fields_filter)
                    if cache_fields_filter_size > fields_filter_size:
                        dis_cache.add(cache_line)
                        continue
                    elif cache_fields_filter_size < fields_filter_size:
                        dis_cache.add(line)
                        continue
                    else:
                        file_writer.writerow(cache_fields)
                        dis_cache.add(line)
                else:
                    cache_fields = EMAIL_SUB_COMMA.split(cache_line)
                    new_cache_fields = cache_fields.copy()
                    new_fields = fields.copy()
                    # compare is same info
                    for i in range(0, CONST.CONDITION_PATT[CONST.LENGTH]):
                        try:
                            cache_v = cache_fields[i]
                            curr_v = fields[i]
                        except IndexError as e:
                            log.exception(
                                "index: {},cache_fields: {},fields: {},error_info: {}".format(i, cache_fields, fields,
                                                                                              str(e)))
                        except Exception:
                            raise Exception

                        if cache_v == curr_v:
                            continue
                        else:
                            if cache_v != "" and curr_v != "":
                                file_writer.writerow(cache_fields)
                                dis_cache.add(line)
                                is_same_info = False
                                break
                            else:
                                if cache_v == "":
                                    new_cache_fields.remove(cache_v)
                                    new_cache_fields.insert(i, curr_v)
                                    continue
                                elif curr_v == "":
                                    new_fields.remove(curr_v)
                                    new_fields.insert(i, cache_v)
                                    continue
                                else:
                                    raise Exception

                    if is_same_info:
                        cache_fields_filter = list(filter(lambda x: x != "", cache_fields))
                        fields_filter = list(filter(lambda x: x != "", fields))
                        cache_fields_filter_size = len(cache_fields_filter)
                        fields_filter_size = len(fields_filter)
                        if cache_fields_filter_size > fields_filter_size:
                            dis_cache.add(cache_line)
                        elif cache_fields_filter_size < fields_filter_size:
                            dis_cache.add(line)
                        else:
                            new_line = ','.join(new_fields)
                            dis_cache.add(new_line)
                    else:
                        pass
        # clear cache
        if len(dis_cache) > 0:
            cache_line = dis_cache.pop()
            cache_fields = EMAIL_SUB_COMMA.split(cache_line)
            file_writer.writerow(cache_fields)


def init_distinct_output_path(input_file_path):
    input_file_name = input_file_path.split('/')[-1]
    file_name = input_file_name.split('.')[0]
    file_path = '/'.join(input_file_path.split('/')[:-1])
    out_put_file_name = '{}_distinct.txt'.format(file_name)
    distinct_dir = '{}/distinct/{}'.format(file_path, file_name)
    clear_dir_file(distinct_dir)
    PathManager.out_put_path = '{}/{}'.format(distinct_dir, out_put_file_name)


def is_same_line(line1: str, line2: str):
    line1 = CACHE_SUB_PATT.sub("", line1)
    line2 = CACHE_SUB_PATT.sub("", line2)
    if line1 == line2:
        return True
    else:
        return False
