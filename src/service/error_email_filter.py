import csv
import os
import re

from redis import Redis

from bean.path_manager import PathManager
from common.const import CONST
from common.globals import Globals
from common.loggers.logger import log
from service.dir_files import get_dir_files, clear_dir_file
from settings.setting import SETTING

AT_PATT = re.compile(r'@')


def remove_error_email(input_dir):
    PathManager.set_black_list()

    if os.path.isdir(input_dir):
        file_list = get_dir_files(input_dir)
        for file_path in file_list:
            clear_filter_dir(file_path)
            filter_error_email(file_path)
    else:
        filter_error_email(input_dir)
        clear_filter_dir(input_dir)


def filter_error_email(input_file_path):
    redis_conn = Globals.get_redis_wrapper().get_redis_conn()
    PathManager.change_output_path_by_operation(input_file_path, operation="email_filter")
    cache_lines = []
    with open(input_file_path, 'r') as file:
        reader = csv.reader(file)
        for fields in reader:
            if len(cache_lines) <= SETTING.CACHE_LINE_SIZE:
                cache_lines.append(fields)
                continue
            else:
                write_filter_cache_lines(cache_lines, redis_conn)
                cache_lines.clear()

        if len(cache_lines) > 0:
            write_filter_cache_lines(cache_lines, redis_conn)
            cache_lines.clear()

    PathManager.write_result()


def is_error_email(email: str):
    """check if email is invalid"""
    log.debug('check email: {}'.format(email))
    if len(email) <= 0:
        return True
    reg_email = re.compile('^[A-Za-z0-9]+[._\\-A-Za-z0-9]*@([A-Za-z0-9]+[-A-Za-z0-9]*.){1,63}[A-Za-z0-9]+$')
    bad_kw = ['spam', 'fuck', 'www', 'stupid', 'asshole', 'suck', 'sexy', 'fuk', 'jerk', 'fake',
              'dummy', 'rubbish', 'kiss', 'mydick', '-.', '.-', 'bigdick', 'bitch', 'blowjob', 'bullshit',
              'cock', 'dumb', 'penis', 'porn', 'pussy', 'shit', 'whore', '.gov.', '.mil.']
    is_error = False
    try:
        email = email.lower()
        for sig in bad_kw:
            if sig.lower() in email:
                log.debug('type 1: {}'.format(email))
                return True
            email_name = email.split('@')[0]
            domain = email.split('@')[1]
            if len(email_name) <= 4 or email_name[-1] == '-' or email_name[-1] == '.' or domain[-1] == '.' or domain[
                0] == '.':
                log.debug('type 2: {}'.format(email))
                is_error = True
            elif reg_email.match(email) is None or '..' in email or email[
                0] == '.' or '--' in email or ' ' in email or '!' in email:
                log.debug('type 3: {}'.format(email))
                is_error = True
            elif '.' not in domain:
                log.debug('type 4: {}'.format(email))
                is_error = True
            elif 'con' in domain:
                log.debug('type 5,error .con: {}'.format(email))
                is_error = True
    except Exception:
        log.error('invalid email:{} '.format(email))
        is_error = True
        if not is_error:
            log.error('valid email:{} '.format(email))
    return is_error


def write_filter_cache_lines(cache_lines, redis_conn: Redis):
    wrong_file = open(PathManager.wrong_put_path, 'a')
    wrong_writer = csv.writer(wrong_file)
    with open(PathManager.out_put_path, 'a') as file:
        for fields in cache_lines:
            email = fields[0]
            redis_conn.incr(CONST.SUM_EMAILS)
            if is_error_email(email):
                redis_conn.incr(CONST.ERROR_EMAILS)
                wrong_writer.writerow(fields)
                continue
            elif email in PathManager.black_list:
                redis_conn.incr(CONST.ERROR_EMAILS)
                wrong_writer.writerow(fields)
                continue
            else:
                line = split_email(email)
                file.write(line)

    wrong_file.close()


def clear_filter_dir(input_file_path):
    input_file_name = input_file_path.split('/')[-1]
    file_path = '/'.join(input_file_path.split('/')[:-1])
    out_put_dir = '{}/{}/{}'.format(file_path, 'email_filter', input_file_name.split('.')[0])
    clear_dir_file(out_put_dir)


def split_email(email: str):
    domain = AT_PATT.split(email)[-1]
    if domain in CONST.EMAIL_COMPANY_DICT:
        company = CONST.EMAIL_COMPANY_DICT[domain]
    else:
        company = 'others'
    line = '{},{},\n'.format(email, company)
    return line
