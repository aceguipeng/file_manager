import re

import time

import os
from bson import json_util

from common.const import CONST
from common.globals import Globals
from common.loggers.logger import log
from service.dir_files import get_dir_files
from settings.setting import SETTING

MONGO_SUB = re.compile('\n')


def output_mongo_db():
    redis_conn = Globals.get_redis_wrapper().get_redis_conn()
    index_num = redis_conn.get(CONST.MONGO_INDEX)
    mongo_counter = redis_conn.get(CONST.MONGO_COUNTER)
    # init index_num and counter
    if not index_num:
        index_num = 0
    else:
        index_num = int(index_num)

    if mongo_counter:
        index_num += int(mongo_counter)
    # index_num = 670876
    db = Globals.get_mongo_wrapper().get_db()
    collection = db[SETTING.COLLECTION_NAME]
    docs_count = collection.count()
    while True:
        try:
            page_size = SETTING.PAGE_SIZE
            docs = collection.find().skip(index_num).limit(page_size)
            if not docs or index_num > docs_count:
                break
            for doc in docs:
                try:
                    with open(CONST.MONGO_OUTPUT, 'a') as file:
                        file.write(json_util.dumps(doc) + '\n')
                        redis_conn.incr(CONST.MONGO_COUNTER)
                except Exception as e:
                    log.exception(str(e))
                    time.sleep(60)
                    index_num = redis_conn.get(CONST.MONGO_COUNTER)
                    break
            index_num += page_size
            redis_conn.set(CONST.MONGO_INDEX, index_num)
        except Exception as e:
            log.exception(str(e))
            index_num = redis_conn.get(CONST.MONGO_COUNTER)
            continue

    redis_conn.set(CONST.MONGO_INDEX, 0)
    redis_conn.set(CONST.MONGO_COUNTER, 0)

    return


def check_match_emails():
    if not os.path.exists(CONST.EMAIL_CHECK_PATH):
        os.makedirs(CONST.EMAIL_CHECK_PATH, exist_ok=True)
    if not os.path.exists(CONST.EMAIL_NOT_MATCH_PATH):
        os.makedirs(CONST.EMAIL_NOT_MATCH_PATH, exist_ok=True)
    db = Globals.get_mongo_wrapper().get_db()
    collection = db[SETTING.COLLECTION_NAME]
    file_path_list = get_dir_files(CONST.EMAIL_CHECK_PATH)
    for file_path in file_path_list:
        with open(file_path, 'r') as f:
            for line in f:
                email = line.strip().lower()
                result = collection.find_one({CONST.EMAIL: email})
                if result is None:
                    log.info('email: {} is not matched'.format(email))
                    with open(CONST.EMAIL_NOT_MATCH_FILE, 'a') as wf:
                        wf.write('{}\n'.format(email))
                else:
                    continue

    log.info('Done!Has checked all email files!')
