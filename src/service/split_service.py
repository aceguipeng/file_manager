import csv

import os
import shutil

import re

from common.const import CONST
from service.dir_files import get_dir_files
from settings.setting import SETTING

PATH_SPLIT_PATT = re.compile(r'/')


def split_by_length(input_dir):
    file_list = get_dir_files(input_dir)
    for file_path in file_list:
        if os.path.isfile(file_path):
            generate_length_file(file_path)
        else:
            pass


def generate_length_file(input_file):
    cache_lines = []
    with open(input_file, 'r', encoding=CONST.LATIN) as file:
        reader = csv.reader(file)

        for fields in reader:
            if len(cache_lines) <= SETTING.CACHE_LINE_SIZE:
                cache_lines.append(fields)
                continue
            else:
                write_length_cache_lines(input_file, cache_lines)
                cache_lines.clear()

        if len(cache_lines) > 0:
            write_length_cache_lines(input_file, cache_lines)
            cache_lines.clear()


def write_length_cache_lines(input_file_path, cache_lines: list):
    length = 0

    length_file_path = change_length_file(input_file_path, length)

    length_file = open(length_file_path, 'a')
    length_writer = csv.writer(length_file)

    for field in cache_lines:
        if len(field) == length:
            length_writer.writerow(field)
        else:
            length = len(field)
            length_file.close()
            length_file_path = change_length_file(input_file_path, length)
            length_file = open(length_file_path, 'a')
            length_writer = csv.writer(length_file)
            length_writer.writerow(field)

    length_file.close()


def change_length_file(input_file_path, length):
    input_file_name = input_file_path.split('/')[-1]
    file_name = input_file_name.split('.')[0]
    file_path = '/'.join(input_file_path.split('/')[:-1])
    out_put_file_name = '{}_{}.txt'.format(file_name, length)
    length_dir = '{}/split_by_length/{}'.format(file_path, file_name)
    if not os.path.exists(length_dir):
        os.makedirs(length_dir, exist_ok=True)
    length_file_path = '{}/{}'.format(length_dir, out_put_file_name)
    return length_file_path


def dispatch_length_file(file_path: str):
    field_length = None
    with open(file_path, 'r', encoding=CONST.LATIN) as file:
        reader = csv.reader(file)

        for fields in reader:
            field_length = len(fields)
            break

    parent_path = '/'.join(PATH_SPLIT_PATT.split(file_path)[:-1])
    file_name = PATH_SPLIT_PATT.split(file_path)[-1]
    new_parent_path = '{}/dispatch/{}'.format(parent_path, field_length)
    new_file_path = '{}/{}'.format(new_parent_path, file_name)
    if not os.path.exists(new_parent_path):
        os.makedirs(new_parent_path, exist_ok=True)
    else:
        pass

    shutil.copyfile(file_path, new_file_path)


def dispatch_files(input_dir):
    file_list = get_dir_files(input_dir)
    for file_path in file_list:
        if os.path.isfile(file_path):
            dispatch_length_file(file_path)
        else:
            pass
