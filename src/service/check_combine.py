# -*- coding: utf-8 -*-
import os
import re

from bean.path_manager import PathManager
from common.const import CONST
from common.loggers.logger import log
from common.utils import trip_line_space
from service.country_service import split_by_country
from service.dir_files import clear_dir_file, get_dir_files, clear_empty_file
from service.distinct import distinct
from settings.setting import SETTING

NAME_SPLIT_PATT = re.compile(r"[\'\-\s&.`/]")
NAME_SUB_PATT = re.compile(r"(\d[a-zA-Z]*)|([^0-9a-zA-Z]+)")
EMAIL_SUB_PATT = re.compile(r'\s')
EMAIL_SPLIT_PATT = re.compile(r'(\^\^)|(\.\s)|(;)')
EMAIL_SUB_COMMA = re.compile(r',')
PIPE_SUB_PATT = re.compile(r'\|!\|')
QUOT_SUB_PATT = re.compile(r'\"')
SPACE_PATT = re.compile(' ')
EMAIL_PATT = re.compile(r'{}'.format(CONST.EMAIL_PATTERN))
PHONE_PATT = re.compile(r'^[0-9]+$')


def check_pattern(row):
    check_ok = True
    pattern_mode = CONST.CONDITION_PATT[CONST.PATTERN_MODE]
    length = CONST.CONDITION_PATT[CONST.LENGTH]
    row_parts = row.split(',')

    for index in range(length):
        try:
            mode = pattern_mode[index]
            field = row_parts[index]
            pattern = CONST.PATTERN_DICT[mode]
            if pattern.fullmatch(field):
                continue
            else:
                check_ok = False
                break
        except IndexError as e:
            log.exception(str(e))
            check_ok = False

    if len(row_parts) != length:
        check_ok = False

    return check_ok


def check_file_format_and_split(input_dir):
    if os.path.isfile(input_dir):
        check_file_split(input_dir)
    else:
        input_file_list = get_dir_files(input_dir)
        for input_file_path in input_file_list:
            check_file_split(input_file_path)


def check_file_split(input_file_path):
    change_to_check_result_path(input_file_path)
    cache_lines = []

    with open(input_file_path, 'r') as file:
        for line in file:
            line = trip_line_space(line, EMAIL_SUB_COMMA)
            if len(cache_lines) <= SETTING.CACHE_LINE_SIZE:
                cache_lines.append(line)
                continue
            else:
                write_cache_lines(cache_lines)
                cache_lines.clear()
        if len(cache_lines) > 0:
            write_cache_lines(cache_lines)
            cache_lines.clear()

    clear_empty_file(PathManager.out_put_dir)


def change_to_check_result_path(input_file_path):
    input_file_name = input_file_path.split('/')[-1]
    file_path = '/'.join(input_file_path.split('/')[:-1])
    file_prefix = input_file_name.split('.')[0]

    out_put_file_name = '{}_{}.txt'.format(file_prefix, CONST.CHECK_OK)
    wrong_file_name = '{}_{}_{}.txt'.format(file_prefix, CONST.CHECK_OK, CONST.WRONG)
    trash_file_name = '{}_{}_{}.txt'.format(file_prefix, CONST.CHECK_OK, CONST.TRASH)

    PathManager.out_put_dir = '{}/{}/{}'.format(file_path, CONST.CHECK_OK, file_prefix)
    clear_dir_file(PathManager.out_put_dir)
    PathManager.out_put_path = '{}/{}'.format(PathManager.out_put_dir, out_put_file_name)
    PathManager.wrong_put_path = '{}/{}'.format(PathManager.out_put_dir, wrong_file_name)
    PathManager.trash_put_path = '{}/{}'.format(PathManager.out_put_dir, trash_file_name)


def check_format_name(line: str):
    files = line.split(CONST.SPLIT)
    if CONST.CONDITION_PATT[CONST.IS_EMAIL_FIRST]:
        start_index = 1

    else:
        start_index = 2
    first_name = files[start_index]
    middle_name = files[start_index + 1]
    last_name = files[start_index + 2]
    new_name = '-'.join(
        NAME_SPLIT_PATT.split(first_name) + NAME_SPLIT_PATT.split(middle_name) + NAME_SPLIT_PATT.split(last_name))
    new_name_list = new_name.split('-')
    new_name_list = list(filter(lambda x: x != "", new_name_list))
    new_name_size = len(new_name_list)
    if new_name_size == 1:
        first_name = new_name_list[0]
        middle_name = ""
        last_name = ""
    elif new_name_size == 2:
        first_name = new_name_list[0]
        middle_name = ""
        last_name = new_name_list[1]
    elif new_name_size >= 3:
        first_name = new_name_list[0]
        middle_name = new_name_list[1]
        last_name = new_name_list[new_name_size - 1]
    new_name_format_list = [first_name, middle_name, last_name]
    new_name_format_list = list(map(lambda x: NAME_SUB_PATT.sub("", x), new_name_format_list))
    new_fields = files[:start_index] + new_name_format_list + files[start_index + 3:]
    new_line = ','.join(new_fields)
    return new_line


def check_format_email(line: str):
    new_lines = []
    files = line.split(CONST.SPLIT)
    email_field = files[0]
    del files[0]
    email_parts = EMAIL_SPLIT_PATT.split(email_field)
    email_parts = list(filter(lambda x: x is not None and EMAIL_SPLIT_PATT.match(x) is None, email_parts))
    email_parts = list(map(lambda x: EMAIL_SUB_PATT.sub("", x), email_parts))
    email_parts = list(map(lambda x: EMAIL_SUB_COMMA.sub(".", x), email_parts))
    if len(email_parts) < 2:
        email_parts = SPACE_PATT.split(email_field)
    if len(email_parts) >= 2:
        for email in email_parts:
            new_files = files.copy()
            new_files.insert(0, email)
            new_line = CONST.SPLIT.join(new_files)
            new_lines.append(new_line)
        new_lines = list(set(new_lines))
    else:
        new_lines.append(line)
    return new_lines


def check_format_website(line: str, website_sub_patt, website_split_patt):
    new_lines = []
    files = line.split(CONST.SPLIT)
    website_field = files[9]
    del files[9]
    website_parts = website_split_patt.split(website_field)
    website_parts = list(filter(lambda x: x is not None, website_parts))
    website_parts = list(map(lambda x: website_sub_patt.sub("", x), website_parts))
    if len(website_parts) >= 2:
        for website in website_parts:
            new_files = files.copy()
            new_files.insert(9, website)
            new_line = CONST.SPLIT.join(new_files)
            new_lines.append(new_line)
        new_lines = list(set(new_lines))
    else:
        new_lines.append(line)
    return new_lines


def write_cache_lines(lines: list):
    output_file = open(PathManager.out_put_path, 'a')
    wrong_file = open(PathManager.wrong_put_path, 'a')
    trash_file = open(PathManager.trash_put_path, 'a')

    for line in lines:
        line = PIPE_SUB_PATT.sub(',', line)
        line = QUOT_SUB_PATT.sub("", line)
        if len(EMAIL_SUB_COMMA.split(line)) <= 2:
            trash_file.write(line)
            continue
        else:
            pass
        if check_pattern(line[:-1]):
            output_file.write(line)
        else:
            row_parts = EMAIL_SUB_COMMA.split(line)

            if row_parts[0] == 'trash' or is_trash_record(row_parts):
                new_line = ','.join(row_parts[1:])
                trash_file.write(new_line)
                continue
            else:
                pass

            # deal:marshall 3rd
            new_line = check_format_name(line)
            if CONST.CONDITION_PATT[CONST.IS_EMAIL_FIRST]:
                new_email_lines = check_format_email(new_line)
            else:
                new_email_lines = [new_line]
            for new_email_line in new_email_lines:
                if check_pattern(new_email_line[:-1]):
                    output_file.write(new_email_line)
                else:
                    result, country_line = is_country_ok(new_email_line)
                    if result:
                        output_file.write(country_line)
                    else:
                        wrong_file.write(country_line)

    output_file.close()
    wrong_file.close()
    trash_file.close()


def is_country_ok(line: str):
    is_ok = False
    line_parts = EMAIL_SUB_COMMA.split(line)
    if len(line_parts) == 10:
        index = 8
    elif len(line_parts) == 12:
        index = 10
    elif len(line_parts) == 11:
        index = 9
        if len(line_parts) != CONST.LENGTH:
            return is_ok, line
    elif len(line_parts) == 13:
        index = 11
    else:
        return is_ok, line

    country = line_parts[index]
    line_parts.remove(country)

    for item in CONST.OK_COUNTRIES:
        if country in item:
            is_ok = True
            line_parts.insert(index, item[country])
            line = ','.join(line_parts)
            break
        else:
            continue
    if check_pattern(line) is None:
        is_ok = False
    else:
        pass
    return is_ok, line


def is_trash_record(row_parts: list):
    is_trash = True
    for field in row_parts:
        if CONST.CONDITION_PATT[CONST.IS_EMAIL_FIRST]:
            if EMAIL_PATT.match(field):
                is_trash = False
                break
        else:
            if PHONE_PATT.match(field):
                is_trash = False
                break

    return is_trash
