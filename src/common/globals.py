# -*- coding:utf-8 -*-
from common.wrappers.etcd_wrapper import EtcdWrapper
from common.wrappers.hbase_wrapper import HbaseWrapper
from common.wrappers.mongo_wrapper import MongoWrapper
from common.wrappers.mysql_wrapper import MysqlWrapper
from common.wrappers.redis_wrapper import RedisWrapper


class __Globals:
    __hbase_wrapper = None
    __hdfs_wrapper = None
    __mysql_wrapper = None
    __redis_queue = None
    __redis_wrapper = None
    __etcd_wrapper = None
    __mongo_wrapper = None

    def __init__(self):
        pass

    def get_mysql_wrapper(self) -> MysqlWrapper:
        return self.__mysql_wrapper

    def set_mysql_wrapper(self, mysql_wrapper):
        self.__mysql_wrapper = mysql_wrapper

    def get_redis_wrapper(self) -> RedisWrapper:
        return self.__redis_wrapper

    def set_redis_wrapper(self, redis_wrapper):
        self.__redis_wrapper = redis_wrapper

    def get_etcd_wrapper(self) -> EtcdWrapper:
        return self.__etcd_wrapper

    def set_etcd_wrapper(self, etcd_wrapper):
        self.__etcd_wrapper = etcd_wrapper

    def get_hbase_wrapper(self) -> HbaseWrapper:
        return self.__hbase_wrapper

    def set_hbase_wrapper(self, hbase_wrapper):
        self.__hbase_wrapper = hbase_wrapper

    def get_mongo_wrapper(self) -> MongoWrapper:
        return self.__mongo_wrapper

    def set_mongo_wrapper(self, mongo_wrapper):
        self.__mongo_wrapper = mongo_wrapper


Globals = __Globals()
