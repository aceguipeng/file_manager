# -*- coding: utf-8 -*-
import os

import pandas as pd
from pyunpack import Archive as Ar

from common.loggers.logger import log
from service.dir_files import get_dir_files


def extract_file_list(input_dir):
    input_file_paths = get_dir_files(input_dir)
    for input_path in input_file_paths:
        extract_file(input_path)


def extract_file(input_path, output_path=None):
    if output_path is None:
        output_path = '/'.join(input_path.split('/')[:-1])
    Ar(input_path).extractall(output_path)


def trip_line_space(line, comma_patt):
    if isinstance(line, str):
        line_parts = comma_patt.split(line)
        new_line_parts = list(map(lambda x: x.strip(), line_parts))
        new_line = '{}\n'.format(','.join(new_line_parts))
        return new_line
    elif isinstance(line, list):
        new_line_parts = list(map(lambda x: x.strip(), line))
        return new_line_parts
    else:
        raise Exception


def xls2csv(input_dir, out_put_dir):
    file_list = get_dir_files(input_dir)
    err_file_name = []
    for file_path in file_list:
        try:
            if os.path.isfile(file_path):
                xls_file_name = file_path.split('/')[-1].split('.')[0]
                log.info('begin to convert: {}.xls'.format(xls_file_name))
                data_xls = pd.read_excel(file_path, index_col=0)
                csv_file_path = '{}/{}.csv'.format(out_put_dir, xls_file_name)
                data_xls.to_csv(csv_file_path, encoding='utf-8')
                log.info('Done! converted: {}.csv'.format(xls_file_name))

        except Exception as e:
            log.exception('Error in convert file: {},error_info: {}'.format(file_path.split('/')[-1], str(e)))
            err_file_name.append('{}.xls'.format(file_path.split('/')[-1]))
            continue
    log.info('all done!!,failed files: {}'.format(err_file_name))
