# -*- coding:utf-8 -*-
import re


class _CONST(object):
    def __setattr__(self, *_):
        raise SyntaxError('Trying to change a constant value')

    # PROJECTNAME

    SYSTEM_NAME = 'split_file'
    SUBSYSTEM_NAME = 'split_file'

    SYSTEM_PROJECT_NAME = 'split_file'
    SUBSYSTEM_PROJECT_NAME = 'split_file'

    MAX_BACK_FILE_NUM = 10
    MAX_BACK_FILE_SIZE = 64 * 1024 * 1024

    UTF_8 = 'utf-8'
    LATIN = 'latin1'

    # etcd
    ETCD_HOST = 'ETCD_SERVICE_SERVICE_HOST'
    ETCD_PORT = 'ETCD_SERVICE_SERVICE_PORT'

    #
    INDEX = 'index'
    PATTERN = 'pattern'
    DISTINCT = 'distinct_result'
    NAME_LIST = ['email', 'first_name', 'middle_name', 'last_name', 'address', 'city', 'state', 'zip', 'country',
                 'phone']
    EMAIL = 'email'
    PYTHON = 'python'
    CHECK_OK = 'check_ok'
    WRONG = 'wrong'

    COUNTRY_PATT_OK_13 = {
        'length': 13,
        'pattern_mode': ['email', 'name', 'name', 'name', 'gender', 'advance', 'advance', 'advance', 'advance', 'zip',
                         'advance', 'country', 'phone'],
        'is_email_first': True
    }

    # email first_name middle_name last_name address city state zip country phone website company_name
    OWNER_PATT = {
        'length': 10,
        'pattern_mode': ['email', 'name', 'name', 'name', 'advance', 'advance', 'advance', 'zip',
                         'country', 'phone'],
        'is_email_first': True
    }

    OWNER_GENDER_PATT = {
        'length': 11,
        'pattern_mode': ['email', 'name', 'name', 'name', 'gender', 'advance', 'advance', 'advance', 'zip',
                         'country', 'phone'],
        'is_email_first': True
    }

    US_VOTERS_SOURCE_PATT = {
        'length': 13,
        'pattern_mode': ['phone', 'voters_email', 'name', 'name', 'name', 'gender', 'dob', 'advance', 'advance',
                         'advance',
                         'zip', 'zip', 'advance'],
        'is_email_first': False
    }

    EMAIL_PATTERN = '^[^@]+@[^@]+$'

    IS_EMAIL_FIRST = 'is_email_first'

    SUM_EMAILS = 'sum_emails'
    ERROR_EMAILS = 'error_emails'

    PATTERN_DICT = {
        'email': re.compile(r'^[^@]+@[^@]+$'),
        'voters_email': re.compile(r'^([^@]+@[^@]+)?$'),
        'name': re.compile('^[a-zA-Z\u00C0-\u00FF]*$'),
        'gender': re.compile(r'^[mf]?$'),
        'dob': re.compile(r'^(\d{4}-\d{2}-\d{2})?$'),
        'advance': re.compile('^[^,]*$'),
        'zip': re.compile(r'^[0-9a-zA-Z]{0,9}|\d{5}-\d{4}$'),
        'country': re.compile(r'^[a-zA-Z]*\s?[a-zA-Z]*\s?[a-zA-Z]*$'),
        'phone': re.compile(r'^[0-9a-zA-Z]*$')
    }

    SPLIT = ','

    CONDITION_PATT = US_VOTERS_SOURCE_PATT
    VOTERS_EMAIL = 'voters_email'
    PATTERN_MODE = 'pattern_mode'
    LENGTH = 'length'

    ZIP_PATTERN = 'zip_pattern'
    COUNTRY = 'country'
    TRASH = 'trash'

    COUNTRY_INDEX = 8
    CITY_INDEX = 5
    ZIP_INDEX = 7
    STATE_INDEX = 6
    STATE_ID_INDEX = 7
    COUNTRY_ID_INDEX = 9

    MONGO_OUTPUT = '/home/data/su_data/aviata_pnr_serialiazed/aviata_pnr_serialiazed.txt'
    MONGO_COUNTER = 'mongo_counter'
    MONGO_INDEX = 'mongo_index'

    COUNTRY_DICT = {
        "us": "us",
        "usa": "us",
        "united states": "us",
        "united states minor outlying islands": "us",
        "united states minor outlying isl": "us",
        "uk": "uk",
        "united kingdom": "uk",
        "unitedkingdom": "uk",
        "united kingdomurugu": "uk",
        "unitedkigdom": "uk",
        "german": "de",
        "germany": "de",
        "ger": "de",
        "de": "de"
    }

    # TASK
    FILE_PROCESS = 'file_process'

    # zip_patt_dict
    ZIP_PATT_DICT = [
        {
            ZIP_PATTERN: r'\d{3}-\d{4}',
            COUNTRY: 'jp'
        },
        {
            ZIP_PATTERN: r'\d{3}-\d{4}',
            COUNTRY: 'jp'
        }
    ]

    OK_COUNTRIES = [{'u.s.a.': 'us'}, {'us minor outlying islands': 'us minor outlying islands'},
                    {'u.s. virgin islands': 'us virgin islands'},
                    {'south georgia / south sandwich isl': 'south georgia/south sandwich isl'},
                    {'st. pierre and miquelon': 'saint pierre and miquelon'}, {'myanmar/burma': 'myanmar/burma'},
                    {'british virgin isl.': 'british virgin islands'},
                    {'british indian ocean territory': 'british indian ocean territory'}, {'st. lucia': 'Saint Lucia'},
                    {'saint pierre and miquelon': 'saint pierre and miquelon'},
                    {'south georgia/south sandwich isl': 'south georgia/south sandwich isl'},
                    {"st. helena": "Saint helena"}, {"Saint helena": "Saint helena"},
                    {"korea (south)": "south korea"}]

    EMAIL_COMPANY_DICT = {
        'gmail.com': 'gmail',
        'yahoo.com': 'yahoo',
        'hotmail.com': 'ms',
        'live.com': 'ms',
        'outlook.com': 'ms',
        'msn.com': 'ms',
        'qq.com': 'tencent'
    }

    # match
    EMAIL_CHECK_PATH = '../data/emails/check'
    EMAIL_NOT_MATCH_PATH = '../data/emails/check/not_match'
    EMAIL_NOT_MATCH_FILE = '../data/emails/check/not_match/not_match_email.txt'


CONST = _CONST()
