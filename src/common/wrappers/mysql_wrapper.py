# -*- coding: UTF-8 -*-
import contextlib
import traceback

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session

from common.loggers.logger import log
from settings.setting import SETTING


class MysqlWrapper(object):
    def __init__(self):
        self.session_maker = None
        self.engine = None
        self.session = None
        self.scope_session = None

    def create_tables(self, base):
        try:
            log.info('creating sql tables.')
            base.metadata.create_all(self.engine)
        except Exception as e:
            log.error('create tables failed {}'.format(str(e)))

    def connect_mysql(self):
        try:
            self.engine = create_engine('{}?charset=utf8'.format(SETTING.URL_MYSQL), encoding='utf-8',
                                        pool_size=SETTING.POOL_SIZE, max_overflow=SETTING.MAX_OVERFLOW)

            session_maker = sessionmaker(bind=self.engine)
            self.scope_session = scoped_session(session_maker)
        except Exception as e:
            log.error(traceback.format_exc())
            raise e

    @contextlib.contextmanager
    def session_scope(self):
        try:
            self.session = self.scope_session()
            yield self.session
            self.session.commit()
        except Exception as e:
            self.session.rollback()
            raise e
        finally:
            self.scope_session.remove()
