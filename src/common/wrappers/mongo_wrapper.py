# -*- coding:utf-8 -*-
import traceback

from pymongo import MongoClient
from pymongo.database import Database

from common.loggers.logger import log
from settings.setting import SETTING


class MongoWrapper(object):
    def __init__(self):
        self.client = None
        self.db = None

    def connect_mongo(self):
        log.info('try to connect mongodb:{}'.format(SETTING.MONGODB_URL))
        try:
            self.client = MongoClient(SETTING.MONGODB_URL)
            self.db = self.client[SETTING.DB_NAME]
        except Exception as e:
            log.error(traceback.format_exc())
            raise e

    def get_db(self)->Database:
        return self.db
