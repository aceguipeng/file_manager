# -*-coding:utf-8-*-

import redis
from redis import Redis

from common.loggers.logger import log


class RedisWrapper(object):
    def __init__(self, host, port, password=None):
        self.redis_conn = None
        if password is None or len(password) < 4:
            self.redis_pool = redis.ConnectionPool(host=host, port=port)
        else:
            self.redis_pool = redis.ConnectionPool(host=host, port=port, password=password)

        log.info('init redis :{} port:{}'.format(host, port))

    def get_redis_conn(self) -> Redis:
        self.redis_conn = redis.Redis(connection_pool=self.redis_pool)
        return self.redis_conn

    def clear_redis_cache(self, name_space):
        result_list = self.redis_conn.keys('{}:*'.format(name_space))
        for i in result_list:
            self.redis_conn.delete(i)
