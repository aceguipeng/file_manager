# -*- coding:utf-8 -*-

import happybase
from happybase import Table, Connection

from common.const import CONST
from common.loggers.logger import log
from settings.setting import SETTING


class HbaseWrapper:
    def __init__(self, hbase_endpoint, timeout):
        self.hbase_endpoint = hbase_endpoint
        self.timeout = timeout
        self.connection_pool = None
        log.info('init hbase: {}'.format(hbase_endpoint))

    def connect(self, table_prefix=None):
        try:
            self.connection_pool = happybase.ConnectionPool(size=SETTING.HBASE_POOL_SIZE, host=self.hbase_endpoint,
                                                            table_prefix=table_prefix, timeout=self.timeout)

        except Exception as error:
            log.exception(str(error))
            raise Exception(str(error))

    def create_table(self, conn: Connection, tbl_name, data_dict):
        try:
            conn.create_table(tbl_name, data_dict)
            log.info('create table: {} successfully'.format(tbl_name))
        except Exception as e:
            log.exception('target table: {} create fail.'.format(tbl_name, str(e)))
            raise Exception(str(e))

    def get_table(self, tbl_name, pool_conn) -> Table:
        try:
            temp_table = pool_conn.table(tbl_name)
            log.debug('target table: {} get successfully.'.format(tbl_name))
            return temp_table
        except Exception as e:
            log.exception('target table: {} get fail.'.format(tbl_name, str(e)))
            raise Exception(str(e))

    def create_all_tables(self):
        with self.connection_pool.connection() as conn:
            tables = conn.tables()
            for site, v in CONST.AMAZON_DOMAIN.items():
                table_name = CONST.HS_DATA.format(site).encode(encoding=CONST.CODE_UTF8)
                if table_name not in tables:
                    self.create_table(conn, table_name, data_dict={
                        CONST.PRODUCT_INFO: dict(),
                        CONST.BASE_INFO: dict()
                    })
                else:
                    log.info('target table {} has been existed'.format(table_name))
