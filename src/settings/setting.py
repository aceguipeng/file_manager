# -*- coding: utf-8 -*-

import configparser
import traceback

from common.const import CONST
from common.loggers.logger import log


class _SETTING(object):
    GLOBAL_PREFIX = '/{}/global/'.format(CONST.SYSTEM_NAME)
    SETTING_PREFIX = '/{}/{}/{}/'.format(CONST.SYSTEM_NAME, CONST.SUBSYSTEM_NAME, CONST.SUBSYSTEM_PROJECT_NAME)

    def __init__(self):
        self.etcd_wrapper = None
        self.base_dir = '.'
        self.configure_file_name = 'configure.ini'

        self.config_list = [
            ('setting_refresh_interval', float, 'common'),
            ('monitor_sleep_time', float, 'common'),
            ('error_wait_time', float, 'common'),
            ('etcd_reconnect_wait_time', float, 'common'),
            ('cache_line_size', int, 'task')
        ]

        self.global_config_list = [
            ('redis_host', str, 'redis'),
            ('redis_port', int, 'redis'),
            ('redis_password', str, 'redis'),
            ('redis_expire', int, 'redis'),
            ('mongodb_url', str, 'mongo'),
            ('collection_name', str, 'mongo'),
            ('db_name', str, 'mongo'),
            ('page_size', int, 'mongo'),
        ]

    def set_config(self, config_list, prefix_key=None):
        config = configparser.RawConfigParser(allow_no_value=True)
        config.read(self.get_configure_file())
        for item in config_list:
            try:
                name, value_type, section = item[0:3]
                func = item[3] if len(item) >= 4 else None

                if prefix_key:
                    key = '{}{}/{}'.format(prefix_key, section, name)
                    default_value = getattr(self, name.upper())
                    value = value_type(
                        self.etcd_wrapper.get(key, default_value))
                    config.set(section, name, value)
                    if func is not None:
                        value = func(value)
                else:
                    value = value_type(config.get(section, name))
                    if func is not None:
                        value = func(value)

                setattr(self, name.upper(), value)
            except Exception as error:
                log.error(str(error))
                log.error(traceback.format_exc())

        try:
            if prefix_key:
                with open(self.get_configure_file(), 'w') as fp:
                    config.write(fp)
        except Exception as error:
            log.error(str(error))
            log.error(traceback.format_exc())

    def set_base_dir(self, base_dir):
        self.base_dir = base_dir

    def get_configure_file(self):
        return self.base_dir + '/' + self.configure_file_name

    def print_settings(self):
        for item in self.config_list:
            try:
                log.debug(
                    '{}: {}'.format(item[0], getattr(self, item[0].upper())))
            except Exception as error:
                log.error(str(error))
                log.error(traceback.format_exc())

        for item in self.global_config_list:
            try:
                log.debug(
                    '{}: {}'.format(item[0], getattr(self, item[0].upper())))
            except Exception as error:
                log.error(str(error))
                log.error(traceback.format_exc())

    def load_cfg_from_local(self):
        try:
            self.set_config(self.config_list)
            self.set_config(self.global_config_list)
        except Exception as error:
            log.error(str(error))
            log.error(traceback.format_exc())

    def load_cfg_from_remote(self):
        try:
            self.set_config(self.config_list, prefix_key=self.SETTING_PREFIX)
            self.set_config(self.global_config_list,
                            prefix_key=self.GLOBAL_PREFIX)
        except Exception as error:
            log.error(str(error))
            log.error(traceback.format_exc())

    def reload_from_local(self):
        self.load_cfg_from_local()
        self.print_settings()

    def reload_from_remote(self, etcd_wrapper=None):
        if etcd_wrapper:
            self.etcd_wrapper = etcd_wrapper

        self.load_cfg_from_remote()
        self.print_settings()


SETTING = _SETTING()
